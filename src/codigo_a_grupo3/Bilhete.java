
package codigo_a_grupo3;

public class Bilhete {
    private String tipodeBilhete;
    private String acesso;
    
    public Bilhete(){
    
    }

    /**
     * @return the tipodeBilhete
     */
    public String getTipodeBilhete() {
        return tipodeBilhete;
    }

    /**
     * @param tipodeBilhete the tipodeBilhete to set
     */
    public void setTipodeBilhete(String tipodeBilhete) {
        this.tipodeBilhete = tipodeBilhete;
    }

    /**
     * @return the acesso
     */
    public String getAcesso() {
        return acesso;
    }

    /**
     * @param acesso the acesso to set
     */
    public void setAcesso(String acesso) {
        this.acesso = acesso;
    }

}
