/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import Model.Bilhete;
import Model.Festival;

public class Palco {
    
    private int lotacao;


    public Palco(int lotacao) {
        this.lotacao = lotacao;
    }
    
    
    public Palco(Palco p){
    setLotacao(p.lotacao);
    }

    /**
     * @return the lotacao
     */
    public int getLotacao() {
        return lotacao;
    }

    /**
     * @param lotacao the lotacao to set
     */
    public void setLotacao(int lotacao) {
        this.lotacao = lotacao;
    }

    @Override
    public String toString() {
        return String.format("Lotação: " +lotacao); //To change body of generated methods, choose Tools | Templates.
    }
    
}

