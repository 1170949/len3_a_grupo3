
package Model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import Model.Festival;
import Model.Palco;

public class Bilhete {
    private String tipodeBilhete;
    private String acesso;

    public Bilhete(String tipodeBilhete, String acesso) {
        this.tipodeBilhete = tipodeBilhete;
        this.acesso = acesso;
    }
    
    
    public Bilhete(Bilhete b){
    setTipodeBilhete(b.tipodeBilhete);
    setAcesso(b.acesso);
    
    }

    /**
     * @return the tipodeBilhete
     */
    public String getTipodeBilhete() {
        return tipodeBilhete;
    }

    /**
     * @param tipodeBilhete the tipodeBilhete to set
     */
    public void setTipodeBilhete(String tipodeBilhete) {
        this.tipodeBilhete = tipodeBilhete;
    }

    /**
     * @return the acesso
     */
    public String getAcesso() {
        return acesso;
    }

    /**
     * @param acesso the acesso to set
     */
    public void setAcesso(String acesso) {
        this.acesso = acesso;
    }

    /**
     * @return the listBilhete
     */
    @Override
    public String toString() {
        return String.format("Tipo de Bilhete: " +tipodeBilhete+ "Acesso: "+acesso); //To change body of generated methods, choose Tools | Templates.
    }

}
