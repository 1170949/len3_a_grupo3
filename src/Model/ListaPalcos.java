/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ListaPalcos {

    private List<Palco> listPalco;
    
    public ListaPalcos() {
        listPalco = new ArrayList<Palco>();
    }
    
    public List<Palco> getListPalco() {
        return listPalco;
    }
    
    public boolean ler(String nomeFicheiro) {
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(nomeFicheiro));
            try {
                listPalco=(List<Palco>)in.readObject();
            } finally {
                in.close();
            }
            return true;
        } catch (IOException|ClassNotFoundException ex) {
            return false;
        }
    }
    
    public boolean guardar(String nomeFicheiro) {
        try {
            ObjectOutputStream out = new ObjectOutputStream(
                    new FileOutputStream(nomeFicheiro));
            try {
                out.writeObject(getListPalco());
            } finally {
                out.close();
            }
            return true;
        } catch (IOException ex) {
            return false;
        }
    }

    public void adicionaPalco(Palco p){
        listPalco.add(p);
    }
    
    
    
    
}
