/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import Model.Bilhete;
import Model.Palco;

/**
 *
 * @author Bruno Sousa
 */
public class Festival {
    
    private List<Palco> listaPalco;
    private List<Bilhete> listaBilhete; 
    private Palco palco;
    private Bilhete bilhete;
    private String nome;

    public Festival(Palco palco, Bilhete bilhete, String nome) {
        this.palco = palco;
        this.bilhete = bilhete;
        this.nome = nome;
    }



    
    
    public void adicionaPalco (Palco p){
        getListaPalco().add(p);
    }
    
    public void adicionaBilhete(Bilhete b){
        getListaBilhete().add(b);
    }

    /**
     * @return the listaPalco
     */
    public List<Palco> getListaPalco() {
        return listaPalco;
    }

    /**
     * @param listaPalco the listaPalco to set
     */
    public void setListaPalco(List<Palco> listaPalco) {
        this.listaPalco = listaPalco;
    }

    /**
     * @return the listaBilhete
     */
    public List<Bilhete> getListaBilhete() {
        return listaBilhete;
    }

    /**
     * @param listaBilhete the listaBilhete to set
     */
    public void setListaBilhete(List<Bilhete> listaBilhete) {
        this.listaBilhete = listaBilhete;
    }

    /**
     * @return the palco
     */
    public Palco getPalco() {
        return palco;
    }

    /**
     * @param palco the palco to set
     */
    public void setPalco(Palco palco) {
        this.palco = palco;
    }

    /**
     * @return the bilhete
     */
    public Bilhete getBilhete() {
        return bilhete;
    }

    /**
     * @param bilhete the bilhete to set
     */
    public void setBilhete(Bilhete bilhete) {
        this.bilhete = bilhete;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    

    
}

