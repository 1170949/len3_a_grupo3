/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Bruno Sousa
 */
public class Publico {
    private String nome;
    private int idade;
    private String nacionalidade;
    private String tipodeBilhete;
    
    private static final String NOME_POR_OMISSAO = "sem nome";
    private static final int IDADE_POR_OMISSAO = 0;
    private static final String BILHETE_POR_OMISSAO = "sem bilhete definido";
    private static final String NACIO_POR_OMISSAO = "sem nacionalidade";

    public Publico(String nome, int idade, String nacionalidade, String tipodeBilhete) {
        this.nome = nome;
        this.idade = idade;
        this.nacionalidade = nacionalidade;
        this.tipodeBilhete = tipodeBilhete;
    }
    
    
    
    public Publico(){
    nome = NOME_POR_OMISSAO;
    idade = IDADE_POR_OMISSAO ;
    tipodeBilhete = BILHETE_POR_OMISSAO;
    nacionalidade = NACIO_POR_OMISSAO;
    
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the idade
     */
    public int getIdade() {
        return idade;
    }

    /**
     * @param idade the idade to set
     */
    public void setIdade(int idade) {
        this.idade = idade;
    }

    /**
     * @return the nacionalidade
     */
    public String getNacionalidade() {
        return nacionalidade;
    }

    /**
     * @param nacionalidade the nacionalidade to set
     */
    public void setNacionalidade(String nacionalidade) {
        this.nacionalidade = nacionalidade;
    }

    /**
     * @return the tipodeBilhete
     */
    public String getTipodeBilhete() {
        return tipodeBilhete;
    }

    /**
     * @param tipodeBilhete the tipodeBilhete to set
     */
    public void setTipodeBilhete(String tipodeBilhete) {
        this.tipodeBilhete = tipodeBilhete;
    }
}
